﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using Xunit;

namespace Livo.MT940Parser.Tests.UtilityParser
{
    public class BalanceParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParseStandardBalance_Should_GetProperBalance"/>
        /// <para>First object:  string - line to parse</para>
        /// <para>Second object: string - decimal separator</para>
        /// <para>Third object:  Balance - expected result</para>
        /// </summary>
        public static IEnumerable<object[]> BalanceData =>
            new List<object[]>
            {
                new object[] {
                    "C050824PLN15,01",
                    ",",
                    new Balance {
                        BalanceSign = DebitCredit.Credit,
                        Date = new DateTime(2005, 8, 24),
                        Currency = "PLN",
                        Amount = 15.01m
                    }},
                new object[] {
                    "C160122PLN185152815,12",
                    ",",
                    new Balance {
                        BalanceSign = DebitCredit.Credit,
                        Date = new DateTime(2016, 1, 22),
                        Currency = "PLN",
                        Amount = 185152815.12m
                    }},
                new object[] {
                    "C160122EUR0,00",
                    ",",
                    new Balance {
                        BalanceSign = DebitCredit.Credit,
                        Date = new DateTime(2016, 1, 22),
                        Currency = "EUR",
                        Amount = 0m
                    }},
            };

        [Theory]
        [MemberData(nameof(BalanceData))]
        public void SWIFT_Should_GetProperBalance(string line, char decimalSeparator, Balance expectedResult)
        {
            Balance balance = MT940Parser.UtilityParser.BalanceParser.SWIFTBalance(line, decimalSeparator);
            Assert.Equal(expectedResult, balance);
        }
    }
}
