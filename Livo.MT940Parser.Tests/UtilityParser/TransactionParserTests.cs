﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using Xunit;

namespace Livo.MT940Parser.Tests.UtilityParser
{
    public class TransactionParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParseStandardTransaction_Should_GetProperTransaction"/>
        /// <para>First object:  string - line to parse</para>
        /// <para>Second object: string - decimal separator</para>
        /// <para>Third object:  Transaction - expected result</para>
        /// </summary>
        public static IEnumerable<object[]> TransactionData =>
            new List<object[]>
            {
                new object[] {
                    "0501120112DN449,77NTRFREFKLI1234567890//BR05012139000001",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2005, 1, 12),
                        EntryDate = new DateTime(2005, 1, 12),
                        ValueSign = DebitCredit.Debit,
                        Value = 449.77m,
                        Type = "NTRF",
                        CustomerReference = "REFKLI1234567890",
                        BankReference = "BR05012139000001"
                    }},
                new object[] {
                    "1601220122DR15827,06N099NONE//FX1602201711",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = new DateTime(2016, 1, 22),
                        ValueSign = DebitCredit.Debit,
                        Value = 15827.06m,
                        Type = "N099",
                        CustomerReference = "NONE",
                        BankReference = "FX1602201711"
                    }},
                new object[] {
                    "160122DR15827,06NFEXNONE//FX1602201711",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = default, // Not present
                        ValueSign = DebitCredit.Debit,
                        Value = 15827.06m,
                        Type = "NFEX",
                        CustomerReference = "NONE",
                        BankReference = "FX1602201711"
                    }},
                new object[] {
                    "1601220122DR15827,06NFEXNONE",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = new DateTime(2016, 1, 22),
                        ValueSign = DebitCredit.Debit,
                        Value = 15827.06m,
                        Type = "NFEX",
                        CustomerReference = "NONE",
                        BankReference = default // Not present
                    }},
                new object[] {
                    "1601220122D15827,06NFEXNONE//FX1602201711",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = new DateTime(2016, 1, 22),
                        ValueSign = DebitCredit.Debit,
                        Value = 15827.06m,
                        Type = "NFEX",
                        CustomerReference = "NONE",
                        BankReference = "FX1602201711"
                    }},
                new object[] {
                    "160122RC15827,06NFEXNONE",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = default, // Not present
                        ValueSign = DebitCredit.ReversalOfCredit,
                        Value = 15827.06m,
                        Type = "NFEX",
                        CustomerReference = "NONE",
                        BankReference = default // Not present
                    }},
                new object[] {
                    "1601220122RDN15827,06NFEXNONE//FX1602201711",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2016, 1, 22),
                        EntryDate = new DateTime(2016, 1, 22),
                        ValueSign = DebitCredit.ReversalOfDebit,
                        Value = 15827.06m,
                        Type = "NFEX",
                        CustomerReference = "NONE",
                        BankReference = "FX1602201711"
                    }},
                new object[] {
                    "0710091009DN4,03NTRFREFER //BR07282102000060",
                    ',',
                    new Transaction
                    {
                        ValueDate = new DateTime(2007, 10, 9),
                        EntryDate = new DateTime(2007, 10, 9),
                        ValueSign = DebitCredit.Debit,
                        Value = 4.03m,
                        Type = "NTRF",
                        CustomerReference = "REFER ",
                        BankReference = "BR07282102000060"
                    }},
                new object[] {
                    "0710091009DN4.03NTRFREFER //BR07282102000060",
                    '.',
                    new Transaction
                    {
                        ValueDate = new DateTime(2007, 10, 9),
                        EntryDate = new DateTime(2007, 10, 9),
                        ValueSign = DebitCredit.Debit,
                        Value = 4.03m,
                        Type = "NTRF",
                        CustomerReference = "REFER ",
                        BankReference = "BR07282102000060"
                    }},
                new object[] {
                    "2402290229DN4.03NTRFREFER //BR07282102000060",
                    '.',
                    new Transaction
                    {
                        ValueDate = new DateTime(2024, 2, 29),
                        EntryDate = new DateTime(2024, 2, 29),
                        ValueSign = DebitCredit.Debit,
                        Value = 4.03m,
                        Type = "NTRF",
                        CustomerReference = "REFER ",
                        BankReference = "BR07282102000060"
                    }},
                new object[] {
                    "2401011229DN4.03NTRFREFER //BR07282102000060",
                    '.',
                    new Transaction
                    {
                        ValueDate = new DateTime(2024, 1, 1),
                        EntryDate = new DateTime(2023, 12, 29),
                        ValueSign = DebitCredit.Debit,
                        Value = 4.03m,
                        Type = "NTRF",
                        CustomerReference = "REFER ",
                        BankReference = "BR07282102000060"
                    }},
            };

        [Theory]
        [MemberData(nameof(TransactionData))]
        public void ParseStandardTransaction_Should_GetProperTransaction(string line, char decimalSeparator, Transaction expectedResult)
        {
            Transaction transaction = MT940Parser.UtilityParser.TransactionParser.SWIFTTransaction(line, decimalSeparator);
            Assert.Equal(expectedResult, transaction);
        }
    }
}
