﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using ParserMT940.BankStatementParser.MBank;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Livo.MT940Parser.Tests.ReportParser.MBank
{
    public class MBankCorporationParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParseMBankCorporationFile"/>
        /// <para>First object:  string - name of file from Samples directory</para>
        /// <para>Second object: ReportStatement[] - expected result from parsing</para>
        /// </summary>
        public static IEnumerable<object[]> MBankCorporationData
            => new List<object[]>
            {
                new object[]
                {
                    @"MBankCorporationSample.txt",
                    "utf-8",
                    new ReportStatement[]
                    {
                        new ReportStatement
                        {
                            TransactionReferenceNumber = "ST050112CYC/1",
                            AccountIdentifier = "PL06114010100000111111001001",
                            SequenceNumber = "8/1",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "PLN",
                                Amount = 60213.04m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2007, 10, 9),
                                    EntryDate = new DateTime(2007, 10, 9),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 2.50m,
                                    Type = "824",
                                    CustomerReference = "NONREF",
                                    BankReference = "BR07282102000059",
                                    SupplementaryDetails = "824-OPŁ. ZA PRZEL. ELIXIR MT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "824 OPŁATA ZA PRZELEW ELIXIR; TNR: 145271016138274.040001"
                                        }
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 449.77m,
                                    Type = "944",
                                    CustomerReference = "SP300",
                                    BankReference = "BR05012139000001",
                                    SupplementaryDetails = "944-PRZEL.KRAJ.WYCH.MT.ELX",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "944 CompanyNet Przelew krajowy; na rach.: 35109010560000000006093440; dla: PHU Test ul.Dolna 1",
                                            "00-950 Warszawa; tyt.: fv 100/2007; TNR: 145271016138277.020002"
                                        },
                                        TransferTitle = "fv 100/2007",
                                        ReceiverName = "PHU Test ul.Dolna 1 00-950 Warszawa",
                                        ReceiverIBAN = "35109010560000000006093440"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 2.50m,
                                    Type = "824",
                                    CustomerReference = "NONREF",
                                    BankReference = "BR05012139000003",
                                    SupplementaryDetails = "824-OPŁ. ZA PRZEL. ELIXIR MT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "824 OPŁATA ZA PRZELEW ELIXIR; TNR: 145271016138247.000001"
                                        }
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 76.03m,
                                    Type = "945",
                                    CustomerReference = "US1234",
                                    BankReference = "BR05012139000003",
                                    SupplementaryDetails = "945-PRZEL.KRAJ.WYCH.MT.ELX.ZUS",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "945 CopmanyNet Przelew ZUS; na rach.: 83101010230000261395100000; rodzaj składki: Ubezpieczenie",
                                            "społeczne; deklaracja S 200708 01; NIP: 12345678901; ID uzup.: R 123456789; ref. Klienta: teścik; TNR:",
                                            "145211001552633.000001"
                                        },
                                        ReceiverIBAN = "83101010230000261395100000"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 2.50m,
                                    Type = "824",
                                    CustomerReference = "NONREF",
                                    BankReference = "BR05012139000004",
                                    SupplementaryDetails = "824-OPŁ. ZA PRZEL. ELIXIR MT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "824 OPŁ. ZA PRZEL. ELIXIR CompanyNet; TNR: 145271016138274.050001"
                                        }
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2007, 10, 9),
                                    EntryDate = new DateTime(2007, 10, 9),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 4.03m,
                                    Type = "945",
                                    CustomerReference = "REFER ",
                                    BankReference = "BR07282102000060",
                                    SupplementaryDetails = "945-PRZEL.KRAJ.WYCH.MT.ELX.ZUS",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "945 CompanyNet PRZELEW ZUS; NA RACH.: 73101010230000261395300000;",
                                            "RODZAJ SKŁADKI: FPIFGSP; DEKLARACJA: D 200708 01; NIP:",
                                            "5555555555; ID UZUP.: R 011834870; NR DEC/UM/TW.: ST 4.3 NUMER",
                                            "DC; REF. KLIENTA: REFER; TNR: 145271016138274.050002"
                                        },
                                        ReceiverIBAN = "73101010230000261395300000"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 40900.0m,
                                    Type = "307",
                                    CustomerReference = "NONE",
                                    BankReference = "FX0501200004",
                                    SupplementaryDetails = "307-TRANSAKCJA WYMIANY WALUT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "307 TRANSAKCJA WYMIANY WALUT; KURS: 4.09; TNR: 145271016138275.000001"
                                        }
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 111.64m,
                                    Type = "835",
                                    CustomerReference = "NONREF",
                                    BankReference = "FT05012999000010",
                                    SupplementaryDetails = "835-PROW. ZA PRZEL. ZAGR. MT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "835 PROW. ZA PRZEL. ZAGR. CompanyNet; OPŁATA ZA SWIFT; TNR: 145271016138276.000001"
                                        }
                                    }
                                },
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "PLN",
                                Amount = 18667.79m
                            },
                            ClosingAvailableBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "PLN",
                                Amount = 18667.79m
                            },
                        },
                        new ReportStatement
                        {
                            TransactionReferenceNumber = "ST050112CYC/1",
                            AccountIdentifier = "PL58114020200000111111001002",
                            SequenceNumber = "8/1",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "EUR",
                                Amount = 0.0m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 10000.0m,
                                    Type = "306",
                                    CustomerReference = "NONE",
                                    BankReference = "FX0501200004",
                                    SupplementaryDetails = "306-TRANSAKCJA WYMIANY WALUT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "306 TRANSAKCJA WYMIANY WALUT; KURS: 4.09; TNR: 145271016138277.000001"
                                        }
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2005, 1, 12),
                                    EntryDate = new DateTime(2005, 1, 12),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 10000.0m,
                                    Type = "983",
                                    CustomerReference = "TOW789",
                                    BankReference = "FT05012999000010",
                                    SupplementaryDetails = "983-POLECENIE WYPŁATY MT",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "983 Polecenie wypłaty wychodzące CompanyNet; na rach.: 35109010560000000006093440; dla:",
                                            "Nazwa Adres; tyt.: ST 1.53; waluta: EUR; kwota: 1,53; kurs: 3,8649; prow. zagr.: PLN25,00; data NOSTRO:",
                                            "11.10.2007; ref. MT103: FT07282102000031; TNR: 145270005844139.000003"
                                        },
                                        TransferTitle = "ST 1.53",
                                        ReceiverName = "Nazwa Adres",
                                        ReceiverIBAN = "35109010560000000006093440"
                                    }
                                },
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "EUR",
                                Amount = 0.0m
                            },
                            ClosingAvailableBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "EUR",
                                Amount = 0.0m
                            },
                        },
                        new ReportStatement
                        {
                            TransactionReferenceNumber = "ST050112CYC/1",
                            AccountIdentifier = "PL31114020200000111111001003",
                            SequenceNumber = "8/1",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "USD",
                                Amount = 42.85m
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "USD",
                                Amount = 42.85m
                            },
                            ClosingAvailableBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2005, 1, 12),
                                Currency = "USD",
                                Amount = 42.85m
                            }
                        }
                    }
                }
            };

        [Theory]
        [MemberData(nameof(MBankCorporationData))]
        public void ParseMBankCorporationFile(string fileName, string encodingName, ReportStatement[] expectedStatements)
        {
            MBankCorporationParser parser = new(',');

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(encodingName);

            string fileContent = Sample.Get(fileName, encoding);

            ReportStatement[] statements = parser.Parse(fileContent).ToArray();

            Assert.Equal(expectedStatements, statements);
        }
    }
}
