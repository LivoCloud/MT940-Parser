﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportParser.ING;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Livo.MT940Parser.Tests.ReportParser.ING
{
    public class INGParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParseINGFile"/>
        /// <para>First object:  string - name of file from Samples directory</para>
        /// <para>Second object: ReportStatement[] - expected result from parsing</para>
        /// </summary>
        public static IEnumerable<object[]> INGData
            => new List<object[]>
            {
                new object[]
                {
                    @"INGSample.txt",
                    "windows-1250",
                    //"utf-8",
                    new ReportStatement[]
                    {
                        new ReportStatement
                        {
                            AccountIdentifier = "PL29105010381000002201994791",
                            SequenceNumber = "00001",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2003, 6, 16),
                                Currency = "PLN",
                                Amount = 123456789101.45m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2003, 6, 16),
                                    EntryDate = new DateTime(2003, 6, 16),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 123456789101.45m,
                                    Type = "076",
                                    CustomerReference = "12345678910",
                                    BankReference = null,
                                    SupplementaryDetails = "KURS 4,0567",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "076/OCMT/USD10001234567,89",
                                            "076",
                                            "~00COCGPRZELEW",
                                            "~20FAKTURA 1/F/03",
                                            "~21FAKTURA 2/F/03",
                                            "~22",
                                            "~23",
                                            "~24",
                                            "~25",
                                            "~2919114020040000350230599137",
                                            "~3011402004",
                                            "~310000350230599137",
                                            "~32 KONTRAHENT SP. Z O.O.",
                                            "~33",
                                            "~34076",
                                            "~38 PL19114020040000350230599137",
                                            "~60OPŁATA ZA PRZELEW 5,00",
                                            "~61KURS 4,0567",
                                            "~62 Ul. GRZYBOWSKA 12",
                                            "~6300-950 WARSZAWA"
                                        },
                                        ReceiverName = "KONTRAHENT SP. Z O.O.",
                                        ReceiverIBAN = "PL19114020040000350230599137",
                                        TransferTitle = "FAKTURA 1/F/03FAKTURA 2/F/03"
                                    }
                                }
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2003, 6, 16),
                                Currency = "PLN",
                                Amount = 123456789102345.00m
                            },
                            ClosingAvailableBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2003, 6, 16),
                                Currency = "PLN",
                                Amount = 123456789102345.00m
                            }
                        }
                    }
                }
            };

        [Theory]
        [MemberData(nameof(INGData))]
        public void ParseINGFile(string fileName, string encodingName, ReportStatement[] expectedStatements)
        {
            INGParser parser = new(',');

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(encodingName);

            string fileContent = Sample.Get(fileName, encoding);

            ReportStatement[] statements = parser.Parse(fileContent).ToArray();

            Assert.Equal(expectedStatements, statements);
        }
    }
}
