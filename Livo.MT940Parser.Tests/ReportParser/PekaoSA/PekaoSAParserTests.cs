﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportParser.PekaoSA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Livo.MT940Parser.Tests.ReportParser.PekaoSA
{
    public class PekaoSAParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParsePekaoSAFile"/>
        /// <para>First object:  string - name of file from Samples directory</para>
        /// <para>Second object: ReportStatement[] - expected result from parsing</para>
        /// </summary>
        public static IEnumerable<object[]> PekaoSAData
            => new List<object[]>
            {
                new object[]
                {
                    @"PekaoSASample.txt",
                    "windows-1250",
                    //"utf-8",
                    new ReportStatement[]
                    {
                        new ReportStatement
                        {
                            AccountIdentifier = "PL61109010140000071219812874",
                            SequenceNumber = "00331/01",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2024, 03, 29),
                                Currency = "PLN",
                                Amount = 7968.33m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2024, 03, 29),
                                    EntryDate = new DateTime(2024, 03, 29),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 841.54m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = "1111111100000000",
                                    SupplementaryDetails = "1,11111E+15",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "051",
                                            "<00PRZELEW INTERNET M/B",
                                            "<10",
                                            "<20FV 11111111, FVKOR 22222222",
                                            "<21, FVKOR 33333333",
                                            "<3010500099",
                                            "<317603123456789123",
                                            "<32Firma QWER Sp. z",
                                            "<33o.o.ul. AkasUlica 22 22-222 A",
                                            "<3810105000997603123456789123",
                                            "<61NONREF"
                                        },
                                        ReceiverName = "Firma QWER Sp. z o.o.ul. AkasUlica 22 22-222 A",
                                        ReceiverIBAN = "10105000997603123456789123",
                                        TransferTitle = "FV 11111111, FVKOR 22222222, FVKOR 33333333"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2024, 03, 29),
                                    EntryDate = new DateTime(2024, 03, 29),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 2.80m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = "M000OCT11111111",
                                    SupplementaryDetails = "M000OCT11111111",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "051",
                                            "<00PROWIZJE AUT.",
                                            "<10",
                                            "<20Op\u0088ata za przelew krajowy",
                                            "<21        z\u0088otowy z rachunku:",
                                            "<22                61109010140",
                                            "<2300007121981287400-x      za",
                                            "<24okres: 29.02.2024-28.03.202",
                                            "<254",
                                            "<3010500099",
                                            "<317603123456789123",
                                            "<3810105000997603123456789123",
                                            "<61NONREF"
                                        },
                                        ReceiverName = null,
                                        ReceiverIBAN = "10105000997603123456789123",
                                        TransferTitle = "Op\u0088ata za przelew krajowy        z\u0088otowy z rachunku:                6110901014000007121981287400-x      zaokres: 29.02.2024-28.03.2024"
                                    }
                                },
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2024, 03, 29),
                                Currency = "PLN",
                                Amount = 7122.79m
                            }
                        }
                    }
                }
            };

        [Theory]
        [MemberData(nameof(PekaoSAData))]
        public void ParsePekaoSAFile(string fileName, string encodingName, ReportStatement[] expectedStatements)
        {
            PekaoSAParser parser = new(',');

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(encodingName);

            string fileContent = Sample.Get(fileName, encoding);

            ReportStatement[] statements = parser.Parse(fileContent).ToArray();

            Assert.Equal(expectedStatements, statements);
        }
    }
}
