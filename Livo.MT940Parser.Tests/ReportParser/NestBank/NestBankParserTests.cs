﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportParser.NestBank;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Livo.MT940Parser.Tests.ReportParser.NestBank
{
    public class NestBankParserTests
    {/// <summary>
     /// Data for test <see cref="ParseNestBankFile"/>
     /// <para>First object:  string - name of file from Samples directory</para>
     /// <para>Second object: ReportStatement[] - expected result from parsing</para>
     /// </summary>
        public static IEnumerable<object[]> NestBankData
            => new List<object[]>
            {
                new object[]
                {
                    @"NestBankSample.txt",
                    //"windows-1250",
                    "utf-8",
                    new ReportStatement[]
                    {
                        new ReportStatement
                        {
                            AccountIdentifier = "PL61109010140000071219812874",
                            SequenceNumber = "231",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2023, 10, 31),
                                Currency = "PLN",
                                Amount = 3160.16m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2023, 10, day: 31),
                                    EntryDate = new DateTime(2023, 10, 31),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 172.20m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = null,
                                    SupplementaryDetails = "23/10/31/111111/1",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "<00Przelewy przychodzace",
                                            "<101392575701",
                                            "<20\"O148/10/2023\"",
                                            "<21",
                                            "<22",
                                            "<23",
                                            "<27TOMASZ ABOWSKI",
                                            "<28",
                                            "<29MARAKUI 21 m. 40",
                                            "<3025300008",
                                            "<312058102347530001",
                                            "<32",
                                            "<3800191010482516025400000001",
                                            "<6002777 WARSZAWA",
                                            "<63REF23/10/31/111111/1",
                                            "020"
                                        },
                                        ReceiverName = "TOMASZ ABOWSKI MARAKUI 21 m. 40 02777 WARSZAWA",
                                        ReceiverIBAN = "00191010482516025400000001",
                                        TransferTitle = "O148/10/2023"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2023, 10, 31),
                                    EntryDate = new DateTime(2023, 10, 31),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 688.80m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = null,
                                    SupplementaryDetails = "23/10/31/222222/1",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "<00Przelewy przychodzace",
                                            "<101392642078",
                                            "<20\"Numer oferty: O7/10/2023 Roger Pi",
                                            "<21otrowski Tlumaczenie z niemieck",
                                            "<22iego na polski\"",
                                            "<23",
                                            "<27KAROLINA MONIKA",
                                            "<28",
                                            "<29OS MADERY 21/37",
                                            "<3010501520",
                                            "<311000009271766819",
                                            "<32",
                                            "<3832101000712223147254000000",
                                            "<6002789 WARSZAWA",
                                            "<63REF23/10/31/222222/1",
                                            "020"
                                        },
                                        ReceiverName = "KAROLINA MONIKA OS MADERY 21/37 02789 WARSZAWA",
                                        ReceiverIBAN = "32101000712223147254000000",
                                        TransferTitle = "Numer oferty: O7/10/2023 Roger Piotrowski Tlumaczenie z niemieckiego na polski"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2023, 10, 31),
                                    EntryDate = new DateTime(2023, 10, 31),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 688.80m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = "24/05/06/92982/11111", // Note: Longer than SWIFT standard allows, but apparently possible in this bank
                                    SupplementaryDetails = "23/10/31/222222/1",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "<00Przelewy przychodzace",
                                            "<101392642078",
                                            "<20\"Numer oferty: O7/10/2023 Roger Pi",
                                            "<21otrowski Tlumaczenie z niemieck",
                                            "<22iego na polski\"",
                                            "<23",
                                            "<27KAROLINA MONIKA",
                                            "<28",
                                            "<29OS MADERY 21/37",
                                            "<3010501520",
                                            "<311000009271766819",
                                            "<32",
                                            "<3832101000712223147254000000",
                                            "<6002789 WARSZAWA",
                                            "<63REF23/10/31/222222/1",
                                            "020"
                                        },
                                        ReceiverName = "KAROLINA MONIKA OS MADERY 21/37 02789 WARSZAWA",
                                        ReceiverIBAN = "32101000712223147254000000",
                                        TransferTitle = "Numer oferty: O7/10/2023 Roger Piotrowski Tlumaczenie z niemieckiego na polski"
                                    }
                                }, // Bank reference longer than SWIFT standard allows
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2023, 10, day: 31),
                                    EntryDate = new DateTime(2023, 10, 31),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 172.20m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = null,
                                    SupplementaryDetails = "23/10/31/111111/1",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "<00Przelewy przychodzace",
                                            "<101392575701",
                                            "<20O148/10/2023",
                                            "<21",
                                            "<22",
                                            "<23",
                                            "<27TOMASZ ABOWSKI",
                                            "<28",
                                            "<29MARAKUI 21 m. 40",
                                            "<3025300008",
                                            "<312058102347530001",
                                            "<32",
                                            "<3800191010482516025400000001",
                                            "<6002777 WARSZAWA",
                                            "<63REF23/10/31/111111/1",
                                            "020"
                                        },
                                        ReceiverName = "TOMASZ ABOWSKI MARAKUI 21 m. 40 02777 WARSZAWA",
                                        ReceiverIBAN = "00191010482516025400000001",
                                        TransferTitle = "O148/10/2023"
                                    }
                                }, // Transaction title w/o quotation marks
                                new ReportStatementData.Transaction 
                                {
                                    ValueDate = new DateTime(2023, 10, day: 31),
                                    EntryDate = new DateTime(2023, 10, 31),
                                    ValueSign = ReportStatementData.DebitCredit.Credit,
                                    Value = 172.20m,
                                    Type = "NTRF",
                                    CustomerReference = "NONREF",
                                    BankReference = null,
                                    SupplementaryDetails = "23/10/31/111111/1",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "<00Przelewy przychodzace",
                                            "<101392575701",
                                            "<20",
                                            "<21",
                                            "<22",
                                            "<23",
                                            "<27TOMASZ ABOWSKI",
                                            "<28",
                                            "<29MARAKUI 21 m. 40",
                                            "<3025300008",
                                            "<312058102347530001",
                                            "<32",
                                            "<3800191010482516025400000001",
                                            "<6002777 WARSZAWA",
                                            "<63REF23/10/31/111111/1",
                                            "020"
                                        },
                                        ReceiverName = "TOMASZ ABOWSKI MARAKUI 21 m. 40 02777 WARSZAWA",
                                        ReceiverIBAN = "00191010482516025400000001",
                                        TransferTitle = null
                                    }
                                }  // No transaction title
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2023, 10, 31),
                                Currency = "PLN",
                                Amount = 4820.66m
                            }
                        }
                    }
                }
            };

        [Theory]
        [MemberData(nameof(NestBankData))]
        public void ParseNestBankFile(string fileName, string encodingName, ReportStatement[] expectedStatements)
        {
            NestBankParser parser = new(',');

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(encodingName);

            string fileContent = Sample.Get(fileName, encoding);

            ReportStatement[] statements = parser.Parse(fileContent).ToArray();

            Assert.Equal(expectedStatements, statements);
        }
    }
}
