﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System.IO;
using System.Reflection;
using System.Text;

namespace Livo.MT940Parser.Tests.ReportParser
{
    internal static class Sample
    {
        internal static string Get(string fileName, Encoding encoding)
        {
            string resourcePath = "Livo.MT940Parser.Tests.ReportParser.Samples." + fileName;

            Assembly assembly = Assembly.GetExecutingAssembly();

            using ( Stream stream = assembly.GetManifestResourceStream(resourcePath) )
            {
                StreamReader reader = new(stream, encoding);
                return reader.ReadToEnd();
            }
        }
    }
}
