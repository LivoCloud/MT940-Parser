﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Text.RegularExpressions;

namespace Livo.MT940Parser.UtilityParser
{
    /// <summary>
    /// Contains parsers for most popular transaction formats
    /// </summary>
    public static class TransactionParser
    {
        /// <summary>
        /// Parses a line to Transaction object asumming most popular format EXCLUDING suplementary details:
        /// <para>6!n[4!n]2a[1!a]15d1!a3!c16x[//16x]</para>
        /// <para>Notice, that due to excluding suplementary details, [34x] in the separate line is not present - function requires only first line of details</para>
        /// <para>Will not work for transactions with value date &lt; year 2000</para>
        /// </summary>
        public static Transaction SWIFTTransaction(string line, char decimalSeparator)
        {
            string escapedDecimalSeparator = Regex.Escape(decimalSeparator.ToString());
            Regex rx = new Regex(@"^(?<valueDate>\d{6})(?<entryDate>\d{4})?(?<debitCreditAndFunds>[a-zA-Z]{0,3})(?<amount>[\d" + escapedDecimalSeparator + @"]{0,15})(?<transactionType>[a-zA-Z]\w{3})(?<customerReference>.{0,16})((?<bankReferenceSlashes>\/\/)(?<bankReference>.{0,16}))?$");
            return CustomTransaction(line, decimalSeparator, rx);
        }

        /// <summary>
        /// Parses a line to Transaction object EXCLUDING suplementary details with provided regex.
        /// <para>Regex should contain the following named groups:</para>
        /// <list type="bullet">
        ///     <item>
        ///         <term>valueDate</term>
        ///         <description>Value date, mandatory</description>
        ///     </item>
        ///     <item>
        ///         <term>entryDate</term>
        ///         <description>Entry date (does not have year), optional</description>
        ///     </item>
        ///     <item>
        ///         <term>debitCreditAndFunds</term>
        ///         <description>0-2 letters of debitCredit, optional letter of funds code (3rd letter of currency code), mandatory</description>
        ///     </item>
        ///     <item>
        ///         <term>amount</term>
        ///         <description>Amount, might contain decimal separator, mandatory</description>
        ///     </item>
        ///     <item>
        ///         <term>transactionType</term>
        ///         <description>Transaction type - 1 letter followed by 3 characters, mandatory</description>
        ///     </item>
        ///     <item>
        ///         <term>customerReference</term>
        ///         <description>Customer reference string, mandatory</description>
        ///     </item>
        ///     <item>
        ///         <term>bankReference</term>
        ///         <description>Bank reference string, optional</description>
        ///     </item>
        /// </list>
        /// <para>This function does not parse supplementory details, so only the first line of transaction should be used</para>
        /// <para>This function will not work for transactions with value date &lt; year 2000</para>
        /// </summary>
        public static Transaction CustomTransaction(string line, char decimalSeparator, Regex regex)
        {
            Transaction transaction = new Transaction();
            string escapedDecimalSeparator = Regex.Escape(decimalSeparator.ToString());
            Match match = regex.Match(line);

            string valueDate = match.Groups[ "valueDate" ].Value; // Value date, mandatory
            string entryDate = match.Groups[ "entryDate" ].Value; // Entry date (does not have year), optional
            string debitCreditAndFunds = match.Groups[ "debitCreditAndFunds" ].Value; // 0-2 letters of debitCredit, optional letter of funds code (3rd letter of currency code), mandatory
            string amount = match.Groups[ "amount" ].Value; // Amount, might contain decimal separator, mandatory
            string transactionType = match.Groups[ "transactionType" ].Value; // Transaction type - 1 letter followed by 3 characters, mandatory
            string customerReference = match.Groups[ "customerReference" ].Value; // Customer reference string, mandatory
            string bankReference = match.Groups[ "bankReference" ].Value; // Bank reference string, optional

            transaction.ValueDate = new DateTime(
                2000 + int.Parse(valueDate.Substring(0, 2)),
                int.Parse(valueDate.Substring(2, 2)),
                int.Parse(valueDate.Substring(4, 2))
                );

            if (!string.IsNullOrWhiteSpace(entryDate))
            {
                // Year is not present, convention says to save 1, but it breaks when there is a leap year so we try to work around it by assigning actual date
                transaction.EntryDate = new DateTime(
                    transaction.ValueDate.Year,
                    int.Parse(entryDate.Substring(0, 2)),
                    int.Parse(entryDate.Substring(2, 2))
                );
                // I am not sure how far the value/entry date can be from each other, but I assume it can be several days, which matters for year change
                // From what I have found, value date can be around 2 days later than entry date, so if its month is 1 and entry date's month is 12, we 
                // assume that value date is from the next year, thus decrementing entry date's year
                if (transaction.ValueDate.Month == 1 && transaction.EntryDate.Month == 12)
                {
                    transaction.EntryDate = transaction.EntryDate.AddYears(-1);
                }
            }

            try // Assume we have only debit/credit
            {
                transaction.ValueSign = DebitCreditFactory.GetDebitCredit(debitCreditAndFunds);
            }
            catch (ArgumentException) // If not - last character is Funds, the rest is sign, at this point in case of exception we want it
            {
                string debitCredit = debitCreditAndFunds.Substring(0, debitCreditAndFunds.Length - 1);
                transaction.ValueSign = DebitCreditFactory.GetDebitCredit(debitCredit);
            }

            amount = amount.Replace(escapedDecimalSeparator[ escapedDecimalSeparator.Length - 1 ], '.');
            transaction.Value = decimal.Parse(amount, System.Globalization.CultureInfo.InvariantCulture);

            transaction.Type = transactionType;
            transaction.CustomerReference = customerReference;

            if (!string.IsNullOrWhiteSpace(bankReference))
            {
                transaction.BankReference = bankReference;
            }

            return transaction;
        }
    }
}
