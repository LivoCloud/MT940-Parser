﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;

namespace Livo.MT940Parser.UtilityParser
{
    /// <summary>
    /// Contains parsers for most popular balance formats
    /// </summary>
    public static class BalanceParser
    {
        /// <summary>
        /// Parses a line to Balance object asumming most popular format: 
        /// <para>1!a6!n3!a15d</para>
        /// <para>Will not work for balances with date &lt; year 2000</para>
        /// </summary>
        public static Balance SWIFTBalance(string line, char decimalSeparator)
        {
            Balance balance = new Balance();
            balance.BalanceSign = DebitCreditFactory.GetDebitCredit(line.Substring(0, 1));
            balance.Date = new DateTime(
                2000 + int.Parse(line.Substring(1, 2)),
                int.Parse(line.Substring(3, 2)),
                int.Parse(line.Substring(5, 2))
                );
            balance.Currency = line.Substring(7, 3);
            string amountWithDecimal = line.Substring(10);
            amountWithDecimal = amountWithDecimal.Replace(decimalSeparator, '.');
            balance.Amount = decimal.Parse(amountWithDecimal, System.Globalization.CultureInfo.InvariantCulture);
            return balance;
        }
    }
}
