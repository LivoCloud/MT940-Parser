﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;
using System.Collections.Generic;

namespace Livo.MT940Parser.ReportStatementData
{
    /// <summary>
    /// Transaction 
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Date of the value.
        /// </summary>
        public DateTime ValueDate { get; set; }

        /// <summary>
        /// Operation, posting date. In the standards year is skipped - so the year is 1 to not provide invalid data.
        /// </summary>
        public DateTime EntryDate { get; set; }

        /// <summary>
        /// Sign of the value, debit for negative, credit for positive.
        /// </summary>
        public DebitCredit ValueSign { get; set; }

        /// <summary>
        /// Value of the operation.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Type of the operation, depends on bank.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Customer reference.
        /// </summary>
        public string CustomerReference { get; set; }

        /// <summary>
        /// Bank reference.
        /// </summary>
        public string BankReference { get; set; }

        /// <summary>
        /// Any additional data about general transaction in form of a string.
        /// </summary>
        public string SupplementaryDetails { get; set; }

        /// <summary>
        /// Details - title and receiver data.
        /// </summary>
        public TransactionDetails Details { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Transaction transaction &&
                   ValueDate == transaction.ValueDate &&
                   EntryDate == transaction.EntryDate &&
                   ValueSign == transaction.ValueSign &&
                   Value == transaction.Value &&
                   Type == transaction.Type &&
                   CustomerReference == transaction.CustomerReference &&
                   BankReference == transaction.BankReference &&
                   SupplementaryDetails == transaction.SupplementaryDetails &&
                   EqualityComparer<TransactionDetails>.Default.Equals(Details, transaction.Details);
        }

        public override int GetHashCode()
        {
            int hashCode = 1291454440;
            hashCode = hashCode * -1521134295 + ValueDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EntryDate.GetHashCode();
            hashCode = hashCode * -1521134295 + ValueSign.GetHashCode();
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Type);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CustomerReference);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(BankReference);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SupplementaryDetails);
            hashCode = hashCode * -1521134295 + EqualityComparer<TransactionDetails>.Default.GetHashCode(Details);
            return hashCode;
        }
    }
}
