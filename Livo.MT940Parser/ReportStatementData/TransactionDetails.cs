﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;
using System.Collections.Generic;

namespace Livo.MT940Parser.ReportStatementData
{
    /// <summary>
    /// Details of the transaction, separated from transaction to better reflect the structure of the MT940 file.
    /// </summary>
    public class TransactionDetails
    {
        /// <summary>
        /// Title of the transfer
        /// </summary>
        public string TransferTitle { get; set; }

        /// <summary>
        /// IBAN of the other party
        /// </summary>
        public string ReceiverIBAN { get; set; }

        /// <summary>
        /// Name and/or address of the other party
        /// </summary>
        public string ReceiverName { get; set; }

        /// <summary>
        /// Raw data of the details, contains all the lines as separate strings
        /// </summary>
        public List<string> RawData { get; set; }

        public override bool Equals(object obj)
        {
            TransactionDetails details = obj as TransactionDetails;
            if ( details == null )
            {
                return false;
            }

            // Both lists must have the same length or both be null
            if ( ( RawData?.Count ?? -1 ) != ( details.RawData?.Count ?? -1 ) )
            {
                return false;
            }

            bool fieldsEqual =
                   TransferTitle == details.TransferTitle &&
                   ReceiverIBAN == details.ReceiverIBAN &&
                   ReceiverName == details.ReceiverName;

            if ( RawData == null || details.RawData == null )
                return fieldsEqual;

            bool rawDataEqual = true;

            for ( int i = 0; i < RawData.Count; i++ )
            {
                rawDataEqual &= RawData[ i ].Equals(details.RawData[ i ]);
            }

            return fieldsEqual & rawDataEqual;
        }

        public override int GetHashCode()
        {
            int hashCode = 648917820;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TransferTitle);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ReceiverIBAN);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ReceiverName);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<string>>.Default.GetHashCode(RawData);
            return hashCode;
        }
    }
}
