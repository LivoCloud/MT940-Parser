﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;
using System.Collections.Generic;

namespace Livo.MT940Parser.ReportStatementData
{
    /// <summary>
    /// Acount balance
    /// </summary>
    public class Balance
    {
        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// ISO Currency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Sign of the balance, credit for positive (or 0), debit for negative.
        /// </summary>
        public DebitCredit BalanceSign { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Balance balance &&
                   Amount == balance.Amount &&
                   Currency == balance.Currency &&
                   Date == balance.Date &&
                   BalanceSign == balance.BalanceSign;
        }

        public override int GetHashCode()
        {
            int hashCode = 538098190;
            hashCode = hashCode * -1521134295 + Amount.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Currency);
            hashCode = hashCode * -1521134295 + Date.GetHashCode();
            hashCode = hashCode * -1521134295 + BalanceSign.GetHashCode();
            return hashCode;
        }
    }
}
