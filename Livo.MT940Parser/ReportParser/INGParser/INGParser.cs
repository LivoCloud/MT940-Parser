﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Livo.MT940Parser.ReportParser.ING
{
    /// <summary>
    /// Parser for the MT940 format of the ING
    /// <para><a href="https://www.ing.pl/_fileserver/item/1122382" /></para>
    /// </summary>
    public class INGParser : SWIFTReportParser
    {
        public INGParser(char decimalSepatator) : base("", "-", decimalSepatator)
        {
        }

        protected override string PreprocessFileContent(string mt940FileContent)
        {
            mt940FileContent = base.PreprocessFileContent(mt940FileContent);

            string[] splitted = mt940FileContent.Split("~".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < splitted.Length; i++)
            {
                if (!splitted[i].EndsWith("\r\n"))
                {
                    splitted[ i ] += "\r\n";
                }
            }

            string joined = string.Join("~", splitted);
            
            return joined;
        }

        protected override void PostprocessReportStatement(ReportStatement reportStatement)
        {
            base.PostprocessReportStatement(reportStatement);

            // Remove "transactions" that are details of the account (it is type 940)
            reportStatement.Transactions.RemoveAll(transaction => transaction.Type == "940");
        }

        protected override bool ContainsHeadersAndTrailers(string mt940FileContent)
        {
            // The fact that trailer is a '-' which is used as a normal character in other places breaks the parser.
            //  Knowing that there is always only 1 statement in a file (according to the specification), the
            //  easiest way to get around this problem is to just ignore header/trailer.
            return false;
        }

        protected override void ParseTag20(ReportCommand command, ReportStatement reportStatement)
        {
            // Do nothing - in this bank, this is just a constant value "MT940"
            return;
        }

        protected override void ParseTag25(ReportCommand command, ReportStatement reportStatement)
        {
            // First character is always '/'
            reportStatement.AccountIdentifier = command.Data[ 0 ].Substring(1);
        }

        protected override void ParseTag61(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag61(command, reportStatement);

            Transaction addedTransaction = reportStatement.Transactions.Last();
            addedTransaction.Type = addedTransaction.Type.Substring(1);
        }


        protected override void ParseTag86(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag86(command, reportStatement);

            StringBuilder titleBuilder = new StringBuilder();
            StringBuilder receiverNameBuilder = new StringBuilder();
            StringBuilder ibanBuilder = new StringBuilder();

            foreach (string line in command.Data)
            {
                if(line.Length < 3) // Very end of the statement which is treated as a part of special 86 tag but it is not
                {
                    continue;
                }

                string lineValue = line.Substring(3);

                // Line is empty - nothing to read
                if (string.IsNullOrEmpty(lineValue))
                {
                    continue;
                }

                if (line.StartsWith("~2") && !line.StartsWith("~29")) //From ~20 to ~28
                {
                    titleBuilder.Append(lineValue);
                }

                if (line.StartsWith("~32") || line.StartsWith("~33"))
                {
                    receiverNameBuilder.Append(lineValue);
                }

                if (line.StartsWith("~38"))
                {
                    ibanBuilder.Append(lineValue);
                }
            }

            Transaction transaction = reportStatement.Transactions.Last();
            TransactionDetails transactionDetails = transaction.Details;

            if (titleBuilder.Length > 0)
                transactionDetails.TransferTitle = titleBuilder.ToString().Trim();
            if (receiverNameBuilder.Length > 0)
                transactionDetails.ReceiverName = receiverNameBuilder.ToString().Trim();
            if (ibanBuilder.Length > 0)
                transactionDetails.ReceiverIBAN = ibanBuilder.ToString().Trim();
        }
    }
}
