﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Livo.MT940Parser.ReportParser.PekaoBP
{
    /// <summary>
    /// Parser for the MT940 format of the Pekao BP
    /// <para><a href="https://www.pkobp.pl/media_files/13c58aca-d24a-4f29-9864-60b12933422d.pdf" /></para>
    /// </summary>
    public class PekaoBPParser : SWIFTReportParser
    {
        public PekaoBPParser(char decimalSepatator) : base("", "-", decimalSepatator)
        {
        }

        protected override bool ContainsHeadersAndTrailers(string mt940FileContent)
        {
            // The fact that trailer is a '-' which is used as a normal character in other places breaks the parser.
            //  Knowing that there is always only 1 statement in a file (according to the specification), the
            //  easiest way to get around this problem is to just ignore header/trailer.
            return false;
        }

        protected override void ParseTag20(ReportCommand command, ReportStatement reportStatement)
        { 
            // Do nothing - in this bank, this is just a constant value "MT940"
            return;
        }

        protected override void ParseTag25(ReportCommand command, ReportStatement reportStatement)
        {
            // First character is always '/'
            reportStatement.AccountIdentifier = command.Data[0].Substring(1);
        }

        protected override void ParseTag61(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag61(command, reportStatement);

            Transaction addedTransaction = reportStatement.Transactions.Last();
            int lenghtOfTypeSubstring = addedTransaction.SupplementaryDetails.IndexOf(' ');
            addedTransaction.Type = addedTransaction.SupplementaryDetails.Substring(0, lenghtOfTypeSubstring);
        }


        private static readonly char[] _emptyLineCharacters = new char[] { (char)255, (char)729 }; // The second one appears in place of ASCII 255 after opening the file as UTF-8
        protected override void ParseTag86(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag86(command, reportStatement);

            StringBuilder titleBuilder = new StringBuilder();
            StringBuilder receiverNameBuilder = new StringBuilder();
            StringBuilder ibanBuilder = new StringBuilder();

            foreach (string line in command.Data)
            {
                string lineValue = line.Substring(3);
                
                // Line is empty - nothing to read
                if (_emptyLineCharacters.Contains(lineValue[0]))
                    continue;
                
                
                if (line.StartsWith("~2")) //From ~20 to ~25, but all other fields start are 30+
                {
                    titleBuilder.Append(lineValue);
                }

                if (line.StartsWith("~32") || line.StartsWith("~33"))
                {
                    receiverNameBuilder.Append(lineValue);
                }

                if (line.StartsWith("~38"))
                {
                    ibanBuilder.Append(lineValue);
                }
            }

            Transaction transaction = reportStatement.Transactions.Last();
            TransactionDetails transactionDetails = transaction.Details;

            if (titleBuilder.Length > 0)
                transactionDetails.TransferTitle = titleBuilder.ToString();
            if (receiverNameBuilder.Length > 0)
                transactionDetails.ReceiverName = receiverNameBuilder.ToString();
            if (ibanBuilder.Length > 0)
                transactionDetails.ReceiverIBAN = ibanBuilder.ToString();
        }
    }
}
