﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser;
using Livo.MT940Parser.ReportParser;
using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParserMT940.BankStatementParser.MBank
{
    /// <summary>
    /// Parser for the MT940 format of the MBank for corporations and MSP
    /// <para><a href="https://www.mbank.pl/pdf/msp-korporacje/bankowosc-elektroniczna/mbank-korpo-opis-formatu-pliku-wyciagow-dziennych-mt940-v-1.4.pdf" /></para>
    /// </summary>
    public class MBankCorporationParser : SWIFTReportParser
    {
        public MBankCorporationParser(char decimalSeparator) : base("\u0001", "-\u0003", decimalSeparator)
        {
        }

        protected override void ParseTag61(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag61(command, reportStatement);

            Transaction addedTransaction = reportStatement.Transactions.Last();
            int lenghtOfTypeSubstring = addedTransaction.SupplementaryDetails.IndexOf('-');
            addedTransaction.Type = addedTransaction.SupplementaryDetails.Substring(0, lenghtOfTypeSubstring);
        }

        protected override void ParseTag86(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag86(command, reportStatement);

            string oneLineDetails = command.Data.Aggregate((previous, next) => previous + " " + next);
            string[] dividedDetails = oneLineDetails.Split(';').Skip(1).ToArray(); // The first element is transaction type
            Dictionary<string, string> details = new Dictionary<string, string>();

            foreach ( string detail in dividedDetails )
            {
                if ( string.IsNullOrWhiteSpace(detail) )
                {
                    continue;
                }

                string[] dividedDetail = detail.Split(new char[] { ':' }, 2);

                if ( dividedDetail.Length == 2 )
                {
                    details.Add(dividedDetail[ 0 ].Trim().ToLowerInvariant(), dividedDetail[ 1 ].Trim());
                }
                else //dividedDetail.Length == 1
                {
                    details.Add(dividedDetail[ 0 ].Trim().ToLowerInvariant(), "");
                }

            }

            Transaction transaction = reportStatement.Transactions.Last();
            TransactionDetails transactionDetails = transaction.Details;

            string dictionaryValue;

            if ( transaction.ValueSign == DebitCredit.Debit )
            {
                if ( details.TryGetValue("na rach.", out dictionaryValue) )
                    transactionDetails.ReceiverIBAN = dictionaryValue;
                if ( details.TryGetValue("to acc.", out dictionaryValue) )
                    transactionDetails.ReceiverIBAN = dictionaryValue;

                if ( details.TryGetValue("tyt.", out dictionaryValue) )
                    transactionDetails.TransferTitle = dictionaryValue;
                if ( details.TryGetValue("details", out dictionaryValue) )
                    transactionDetails.TransferTitle = dictionaryValue;

                if ( details.TryGetValue("dla", out dictionaryValue) )
                    transactionDetails.ReceiverName = dictionaryValue;
                if ( details.TryGetValue("for", out dictionaryValue) )
                    transactionDetails.ReceiverName = dictionaryValue;
            }

            if ( transaction.ValueSign == DebitCredit.Credit )
            {
                if ( details.TryGetValue("z rach.", out dictionaryValue) )
                    transactionDetails.ReceiverIBAN = dictionaryValue;
                if ( details.TryGetValue("from acc.", out dictionaryValue) )
                    transactionDetails.ReceiverIBAN = dictionaryValue;

                if ( details.TryGetValue("tyt.", out dictionaryValue) )
                    transactionDetails.TransferTitle = dictionaryValue;
                if ( details.TryGetValue("details", out dictionaryValue) )
                    transactionDetails.TransferTitle = dictionaryValue;

                if ( details.TryGetValue("od", out dictionaryValue) )
                    transactionDetails.ReceiverName = dictionaryValue;
                if ( details.TryGetValue("from", out dictionaryValue) )
                    transactionDetails.ReceiverName = dictionaryValue;
            }

            return;
        }
    }
}
