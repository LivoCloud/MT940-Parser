﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System.Collections.Generic;

namespace Livo.MT940Parser.ReportParser
{
    /// <summary>
    /// Class storing single command
    /// </summary>
    public class ReportCommand
    {
        /// <summary>
        /// Tag of the command (without the :)
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Data of the command, each entry is one line of data
        /// </summary>
        public List<string> Data { get; set; }
    }
}
