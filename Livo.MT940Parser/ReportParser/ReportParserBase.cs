﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Livo.MT940Parser.ReportParser
{
    public abstract class ReportParserBase
    {
        /// <summary>
        /// Potential statement header used if there are multiple statements in the file
        /// </summary>
        protected readonly string _statementHeader;

        /// <summary>
        /// Potential statement trailer used if there are multiple statements in the file
        /// </summary>
        protected readonly string _statementTrailer;

        /// <summary>
        /// Creates a parser for a mt940 raport.
        /// </summary>
        public ReportParserBase(string statementHeader, string statementTrailer)
        {
            _statementHeader = statementHeader;
            _statementTrailer = statementTrailer;
        }

        /// <summary>
        /// Parses mt940 file content into a collection of statements.
        /// </summary>
        /// <param name="mt940FileContent">Content of the file with mt940 raport.</param>
        /// <exception cref="ArgumentException">File content is empty or null.</exception>
        public ICollection<ReportStatement> Parse(string mt940FileContent)
        {
            if ( string.IsNullOrWhiteSpace(mt940FileContent) )
            {
                throw new ArgumentException("File content is empty or null.", nameof(mt940FileContent));
            }

            string preprocessedFileContent = PreprocessFileContent(mt940FileContent);

            string[] statements;
            if ( ContainsHeadersAndTrailers(preprocessedFileContent) )
            {
                statements = SplitIntoSingleStatements(preprocessedFileContent);
            }
            else
            {
                statements = new string[] { preprocessedFileContent };
            }

            ICollection<ReportStatement> reportStatements = new List<ReportStatement>();

            foreach ( string statementContent in statements )
            {
                ReportStatement reportStatement = ParseStatement(statementContent);
                PostprocessReportStatement(reportStatement);
                reportStatements.Add(reportStatement);
            }

            return reportStatements;
        }

        /// <summary>
        /// Parses a single statement into a ReportStatement object.
        /// </summary>
        public ReportStatement ParseStatement(string statementContent)
        {
            ReportStatement reportStatement = new ReportStatement();
            ReportCommand[] commands = DivideStatementIntoCommands(statementContent);

            foreach ( ReportCommand command in commands )
            {
                switch ( command.Tag )
                {
                    case "20":
                        ParseTag20(command, reportStatement);
                        break;
                    case "21":
                        ParseTag21(command, reportStatement);
                        break;
                    case "25":
                        ParseTag25(command, reportStatement);
                        break;
                    case "28":
                        ParseTag28(command, reportStatement);
                        break;
                    case "28C":
                        ParseTag28C(command, reportStatement);
                        break;
                    case "60a":
                        ParseTag60a(command, reportStatement);
                        break;
                    case "60m":
                        ParseTag60m(command, reportStatement);
                        break;
                    case "60F":
                        ParseTag60F(command, reportStatement);
                        break;
                    case "60M":
                        ParseTag60M(command, reportStatement);
                        break;
                    case "61":
                        ParseTag61(command, reportStatement);
                        break;
                    case "62a":
                        ParseTag62a(command, reportStatement);
                        break;
                    case "62m":
                        ParseTag62m(command, reportStatement);
                        break;
                    case "62F":
                        ParseTag62F(command, reportStatement);
                        break;
                    case "62M":
                        ParseTag62M(command, reportStatement);
                        break;
                    case "64":
                        ParseTag64(command, reportStatement);
                        break;
                    case "65":
                        ParseTag65(command, reportStatement);
                        break;
                    case "86":
                        ParseTag86(command, reportStatement);
                        break;
                    default:
                        break;
                }
            }

            return reportStatement;
        }

        /// <summary>
        /// Divides statement (without header nor trailer) into commands.
        /// </summary>
        private static ReportCommand[] DivideStatementIntoCommands(string statementContent)
        {
            string[] lines = statementContent.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            List<ReportCommand> commands = new List<ReportCommand>();

            foreach ( string line in lines )
            {
                var trimmedLine = line.Trim();
                if ( String.IsNullOrWhiteSpace(trimmedLine) )
                {
                    continue;
                }

                if ( trimmedLine.StartsWith(":") ) // Line starts with ':' - new command
                {
                    ReportCommand command = new ReportCommand();

                    string[] split = trimmedLine.Split(new char[] { ':' }, 3);

                    //[0] - empty, [1] - tag, [2] - first line of data (or empty)
                    command.Tag = split[ 1 ];

                    command.Data = new List<string>();
                    if ( !string.IsNullOrEmpty(split[ 2 ]) )
                    {
                        command.Data.Add(split[ 2 ].Trim());
                    }

                    commands.Add(command);
                }
                else // Line does not start with ':' - add data to current command
                {
                    commands.Last().Data.Add(trimmedLine);
                }

            }

            return commands.ToArray();
        }

        /// <summary>
        /// Preprocesses the file content before parsing.
        /// </summary>
        /// <param name="mt940FileContent"></param>
        protected virtual string PreprocessFileContent(string mt940FileContent)
        {
            return mt940FileContent;
        }

        /// <summary>
        /// Additional processing to the statement after parsing process
        /// </summary>
        protected virtual void PostprocessReportStatement(ReportStatement reportStatement) 
        {
        
        }

        /// <summary>
        /// Checks if file has headers and trailers to possibly split it into multiple statements.
        /// <para>Default implementation: File starts with header</para>
        /// </summary>
        protected virtual bool ContainsHeadersAndTrailers(string mt940FileContent)
        {
            return mt940FileContent.StartsWith(_statementHeader);
        }

        /// <summary>
        /// Splits file with possibly multiple statements into a list of statements without headers and trailers.
        /// <para>Default implementation: Splits file by header and trailer, removing empty lines and whitespace at the begininng and the end of the statements.</para>
        /// </summary>
        protected virtual string[] SplitIntoSingleStatements(string mt940FileContent)
        {
            return mt940FileContent
                .Split(new string[] { _statementHeader, _statementTrailer }, StringSplitOptions.RemoveEmptyEntries)
                .Where(statement => !string.IsNullOrWhiteSpace(statement))
                .Select(statement => statement.Trim())
                .ToArray();
        }

        /// <summary>
        /// Parses tag 20 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag20(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 21 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag21(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 25 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag25(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 28 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag28(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 28C and modifies report statement.
        /// </summary>
        protected virtual void ParseTag28C(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 60a and modifies report statement.
        /// </summary>
        protected virtual void ParseTag60a(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 60m and modifies report statement.
        /// </summary>
        protected virtual void ParseTag60m(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 60F and modifies report statement.
        /// </summary>
        protected virtual void ParseTag60F(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 60M and modifies report statement.
        /// </summary>
        protected virtual void ParseTag60M(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 61 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag61(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 62a and modifies report statement.
        /// </summary>
        protected virtual void ParseTag62a(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 62m and modifies report statement.
        /// </summary>
        protected virtual void ParseTag62m(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 62F and modifies report statement.
        /// </summary>
        protected virtual void ParseTag62F(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 62M and modifies report statement.
        /// </summary>
        protected virtual void ParseTag62M(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 64 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag64(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 65 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag65(ReportCommand command, ReportStatement reportStatement) { }

        /// <summary>
        /// Parses tag 86 and modifies report statement.
        /// </summary>
        protected virtual void ParseTag86(ReportCommand command, ReportStatement reportStatement) { }
    }
}
