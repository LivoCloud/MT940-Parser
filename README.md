# MT940 Parser
Parser for the SWIFT MT940 format that allows for adjustments for non-standard specificaitons of different banks.

## Description
Project was created to be a part of another commercial application, [LivoLink](https://livolink.pl/), and released as standalone open source library. We decided to do that, because no other open source parser was able to parse files from banks with specification slightly different then SWIFT specification and filling the details. This project tries to overcome that problem by implementing a library where users can provide their own logic of parsing particular tags and create parser for the whole file by **mixing and matching parsers for particular tags**.

## How to use?
1. Take an instance of class derived from `Livo.MT940Parser.ReportParser.ReportParserBase`.
1. Call function `Parse` with string (mt940 file content) as an argument.
1. Use collection of objects of type `ReportStatement` as you wish.

For example:
```cs
string mt940FileContent = File.ReadAllText("./report.txt");
var parser = new MBankCorporationParser(); // Implemented parser
ICollection<ReportStatement> statements = parser.Parse(mt940FileContent);
```

## How to write my own parser?
1. Create new class that inherits from `Livo.MT940Parser.ReportParser.ReportParserBase`.
1. Override functions `ContainsHeadersAndTrailers` and `SplitIntoSingleStatements` (or leave default implementation, if header and trailer of the statement is simple constant string - thay are passed in the constructor).
1. Override functions parsing particular tags: `ParseTagX`. All of those functions have 2 arguments - `ReportCommand` which contains tag and all te data, and `ReportStatement` which tag parser should modify.
1. Not overrided tags by default do nothing.

### Which tags can I override?
There are functions `ParseTagX` for tags (replace `X` with them): `20, 21, 25, 28, 28C, 60a, 60m, 60F, 60M, 61, 62a, 62m, 62F, 62M, 64, 65, 86`.

## Preimplemented elements
### Report parsers:
* `Livo.MT940Parser.ReportParser.SWIFTReportParser` - class derived from `ReportParserBase` with all tags matched to tags from `SWIFTTagParser` (all variations of a tag are using the same tag parser). It is a nice base for parsers that are generally following the standard but have only small differences or need additional work for example in tag 86.
* `Livo.MT940Parser.ReportParser.MBank.MBankCorporationParser` - parser (derived from `SWIFTReportParser`) which implements [MBank for corporations and MSP standard](https://www.mbank.pl/pdf/msp-korporacje/bankowosc-elektroniczna/mbank-korpo-opis-formatu-pliku-wyciagow-dziennych-mt940-v-1.4.pdf) (ie. switches transaction type to concrete MBank defined type and fills details of the transaction from most of those types).
* `Livo.MT940Parser.ReportParser.PekaoBP.PekaoBPParser` - parser (derived from `SWIFTReportParser`) which implements [PKO BP standard](https://www.pkobp.pl/media_files/13c58aca-d24a-4f29-9864-60b12933422d.pdf) (ie. fills details of the transaction). Note that this parser should work with multiple banks (at least in Poland) sice it does things in the most popular way.
* `Livo.MT940Parser.ReportParser.PekaoBP.INGParser` - parser (derived from `SWIFTReportParser`) which implements [ING standard](https://www.ing.pl/_fileserver/item/1122382) (ie. fills details of the transaction).
* `Livo.MT940Parser.ReportParser.NestBank.NestBankParser` - parser (derived from `SWIFTReportParser`) which fills details of the transaction based on analyzed statements (no documentation was found).
* `Livo.MT940Parser.ReportParser.PekaoSA.PekaoSAParser` - parser (derived from `SWIFTReportParser`) which fills details of the transaction based on analyzed statements (found documentation did not match statements that we received to analyze).

### Tag parsers: 
* `Livo.MT940Parser.ReportParser.TagParser.SWIFTTagParser` - static class containing parsers for tags `20, 21, 25, 28, 60, 61, 62, 64, 65, 86` according to [this description of SWIFT format](https://www.sepaforcorporates.com/swift-for-corporates/account-statement-mt940-file-format-overview/).

### Additional elements:
* `Livo.MT940Parser.UtilityParser.BalanceParser` - static class containing parsers to class `Balance`, with purpose of being used in tags `60`, `62`, `64` and `65`.
* `Livo.MT940Parser.UtilityParser.TransactionParser` - static class containing parsers to class `Transaction`, with purpose of being used in tag `61`.

## Changelog
* 1.0.0 to 1.0.2 - Initial release and hot fixes.
* 1.0.3 - Added PKO BP parser.
* 1.0.4 - Added ING parser.
* 1.0.5 - Added Nest Bank parser.
* 1.0.6 - Fixed leap year bug.
* 1.0.7 - Added PekaoSA parser.
* 1.0.8 - Allow non-standard tag 61 transaction parser, fixed Nest Bank parser which uses non-standard tag 61 transactions sometimes.
* 1.0.9 - Fixed a quirk for Nest Bank statements - sometimes statememts does not use quotation marks in transaction title, sometimes are straight up empty.